SHELL := /bin/bash

build:
	gcc rhcalc.c -o bin/rhcalc
install:
	make build
	chmod +x bin/rhcalc
	sudo cp bin/rhcalc /usr/bin
cross-compile:
	i686-w64-mingw32-gcc rhcalc.c -o bin/rhcalc_win32.exe
test:
	make build
	bin/rhcalc

generate_alias:
	chmod +x bin/rhcalc
	echo "alias rhcalc='"$(realpath bin/rhcalc)"'">>~/.bashrc
	source ~/.bashrc

