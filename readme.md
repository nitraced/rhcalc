# Routh-Hurwitz Calculator

By Justin Cano, Polytechnique Montréal (QC,Canada) and ISAE-Supaéro (France)

## Installation

In this repository you will find the source `rhcalc.c` you can compile your own version of **rhcalc** software or modify this code as your convinience... 

### Windows users

Please use pre-compiled binary situated in the `bin/` folder.

### Linux users

#### Compiling with GCC

You can re-compile the binary using the following make command invoking GCC compilator:

```sh
make build
```

and then run the code performing `./rhcalc` in the directory containing the binary. Pay attention to the "executablilty" of your program : if you have rights problems then you can use `chmod +x rhcalc` to allow the execution of our program.

#### Installation of the software invokable anywhere (sudoers)

You can also install the code within your computer using this command :

```sh
make install
```

#### Installation of the software invokable anywhere but... locally!

Alternatively to the classical installation, which requires `sudo` rights, we can setup a `.bashrc` alias providing the executable path. To do so, type :
 
```sh
make generate_alias
```
even in another directory the command `rhcalc` is therafer recognized! 

## Pre-compiled binaries

Two precompiled binaries are there and ready to use by your machine :

- `bin/rhcalc` : binary compiled on Ubuntu *Bionic Beaver* 18.04 LTS.
- `bin/rhcalc32.exe` : binary usable by any `Windows OS` supporting 32-bits. Simple double-click on it and it will be executed within the Windows Shell.

## Usage of the software

The first dialogue interface will require you to enter the n+1 coefficients a_i of the denominator D(s) of the studied transfer function H(s) as follows
```math
D(s)=a_ns^n+...+a_is^i+a_1s+a_0.
```
Once it's correctly done it will perform Routh-Hurwitz table calculus and will interpret the table with [Routh-Hurwitz stability criterion](https://en.wikipedia.org/wiki/Routh%E2%80%93Hurwitz_stability_criterion).

H(s) is stable if and only if those two conditions are respected :

1. All coefficients a_i are of the same sign;
2. The number within column of Routh-Hurwitz table are strictly of the same sign.

If the table has a zero in the first column then we have to perform the test on perturbated RH-table defined below :
```math
D(s+\epsilon) \approx D(s) + \frac{\partial D}{\partial s} (s)\epsilon
```
Depending on the sign of  epsilon, we can conclude to **marginal stabiltity** or not :

- Two tables have to be calculated depending of the sign of epsilon;
- If <u>independantly of epsilon sign</u>, a sign change occurs in the first column of RH tables then the **system is unstable**. If there are no sign changes then the **system is marginally stable**. 
- If   <u>dependantly of epsilon sign</u>, we note a sign change in one table and not in the second one, then we have a pair of pole on the imaginary axis. This will not affect **marginal stability** (oscillators are BIBO stable).

##### Remark

For the trivial case where the first coefficient of the polynomial is null, $`a_0=0`$, then **the system is unstable** because one integrator is present (zero is a pole of the system!).

## Licence

This software is under MIT License.

#### Acknowledgements 

Thanks to my Ph.D. candidate friend Catherine for her encouraging and numerous "*Hé Boboys*" during the development of this tool.

I warmly thank Giorgio Minarelli to have found two bugs in the source-code and his corrections advices. 
