/*
 * rhcalc.c
 *
 * Copyright 2019 jcano <jcano@yabloko>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#define POLYNOMIAL_SIZE_LIMIT 50

// Global variable for polynomes
static double coefficients[POLYNOMIAL_SIZE_LIMIT + 1];
static double derivative_coeffs[POLYNOMIAL_SIZE_LIMIT + 1];
static double taylor_plus[POLYNOMIAL_SIZE_LIMIT + 1];
static double taylor_minus[POLYNOMIAL_SIZE_LIMIT + 1];

// Our routhtable
typedef struct routhtable {
	double tab[POLYNOMIAL_SIZE_LIMIT + 1][POLYNOMIAL_SIZE_LIMIT + 1];
	uint32_t n_row;
	uint32_t n_column;
} routhtable;

// Private prototypes
int dimension_handle(void);
void coefficient_querry(uint32_t);
routhtable routh_filling(uint32_t, double*);
void print_rh(routhtable,uint8_t);
routhtable routh_calcus(routhtable);
int routh_interpretation(routhtable, uint32_t);
void derivative_calc(uint32_t);
void taylor_expansion(uint32_t);
int singular_interpretation(uint32_t);

// Main routhine ;)
int main(int argc, char **argv) {
	printf(
			"Welcome to Routh-Hurtwitz Calculator \nA little RH script created by Justin Cano, Polytechnique Montreal \n ------- \n ");
	uint32_t N = dimension_handle();
	coefficient_querry(N);
	printf("\nRH-tabular is filled at first with D(s) coefficients :\n");
	routhtable mytable = routh_filling(N, coefficients);
	print_rh(mytable,1);
	printf("Then, we perform the calculum of the table  :\n");
	mytable = routh_calcus(mytable);
	print_rh(mytable,0);
	routh_interpretation(mytable, N);
	return 0;
}

int dimension_handle(void) {
	uint32_t N;
	printf(
			"\n \nEnter the degree of the polynomial denominator of the transfer function: \n D(s)=a_Ns^N + ... + a_1s + a_0\n");
	scanf("%d", &N);
	if (N > POLYNOMIAL_SIZE_LIMIT) {
		printf("Error! dimension is too important");
		exit(1);
	}
	return N;
}

void coefficient_querry(uint32_t N) {
	double coeff;
	int key = 0;
	while (key == 0) {
		for (int i = N; i >= 0; i--) {
			printf("\nEnter coefficient a_%d :", i);
			scanf("%lf", &coeff);
			coefficients[i] = coeff;
		}
		printf("Do you want to examine the following polynomial: \nD(s)=");
		for (int i = N; i > 0; i--) {
			printf("%.2lf s^%d+", coefficients[i], i);
		}
		printf("%.2lf  ?\n Type 1:Yes 0:No \n", coefficients[0]);
		scanf("%d", &key);
	}
}

routhtable routh_filling(uint32_t N, double *tab) {
	routhtable myrouth;
	int i, r, c;
	myrouth.n_column = ((int) N / 2) + 1;
	myrouth.n_row = N + 1;
	double mytab[myrouth.n_row][myrouth.n_row];
	r = N;
	c = 0;
	for (i = N; i >= 0; i--) {
		myrouth.tab[r][c] = tab[i];
		if (r == N) {
			r = N - 1;
		} else {
			r = N;
			c++;
		}
	}
	return myrouth;
}

void print_rh(routhtable myrouth, uint8_t fill_table) {
	int N, r, c;
	char str[20];
	N = myrouth.n_row - 1;

	for (r = N; r >= 0; r--) {
		sprintf(str, "s^%d", r);
		printf("%8s :", str);
		for (c = 0; c < myrouth.n_column; c++) {
			// For small values display 0.01 instead of 0.00
			if (fabs(myrouth.tab[r][c])
					< 0.01&& myrouth.tab[r][c]!=0 && myrouth.tab[r][c]!=INFINITY) {
				if (myrouth.tab[r][c] > 0) {
					sprintf(str, "%.2f", 0.01);
				} else {
					sprintf(str, "%.2f", -0.01);
				}
			} else {
				sprintf(str, "%.2f", myrouth.tab[r][c]);
			}
			printf("%10s |", str);
		}
		printf("\n");
		if (myrouth.tab[r][0] == 0.0 || (fill_table == 1 && r < N)) {
			// Stop the display if a zero appears or the initial filling of the table 
			break;
		}
	}
}

routhtable routh_calcus(routhtable myrouth) {
	int i, j, N, M;
	N = myrouth.n_row - 1 - 2;
	M = myrouth.n_column - 1 - 1;
	for (i = N; i >= 0; i--) {
		for (j = 0; j <= M; j++) {
			if (myrouth.tab[i + 1][0] == 0 || myrouth.tab[i + 1][0] == INFINITY) {
				// In case of a zero appearance 
				myrouth.tab[i][j] = INFINITY;
			} else {
				// Classical det-based formula
				myrouth.tab[i][j] = (myrouth.tab[i + 1][0]
						* myrouth.tab[i + 2][j + 1]
						- myrouth.tab[i + 2][0] * myrouth.tab[i + 1][j + 1])
						/ myrouth.tab[i + 1][0];
			}
		}
		// The column at the right of the table = zeros, calculus can be skipped [thanks to G.Minarelli!]
		if (M != 0 && i % 2 == 1) {
			M--;
		}
	}
	return myrouth;
}

int routh_interpretation(routhtable myrouth, uint32_t N) {
	int i, count, singular;
	float minus;
	printf("Let's examine the first column of our RH-table : \n");

	count = 0;
	singular = 0;

	if (myrouth.tab[myrouth.n_row - 1][0] > 0) {
		minus = 1.0;
	} else {
		minus = -1.0;
	};
	for (i = myrouth.n_row - 1; i >= 0; i--) {
		if (minus * myrouth.tab[i][0] < 0.0) {
			minus *= -1;
			count++;
		}
		if (myrouth.tab[i][0] == INFINITY && count == 0) {
			printf(
					"A zero has appeared in the firt column: this system seems to be unstable or marginal stable...  \n \n");
			// Call for the magic function...
			singular_interpretation(N);
			singular = 1;
			break;
		}
	}

	if (singular == 0) {
		if (count > 0) {
			printf(
					"We have %d sign changes within the first column : the system is unstable!",
					count);
		} else {
			printf(
					"We have none sign changes within the first column : the system is stable!");
		}
	}
	printf(
			"\n \n ---------------------- \n Enter any value to exit the program \n");
	scanf("%d", &i);
	return 0;
}

/* Handles for the singular case */

// Computes the derivative of D
void derivative_calc(uint32_t N) {
	int i;
	for (i = 0; i < N; i++) {
		derivative_coeffs[i] = coefficients[i + 1] * (i + 1.0);
	}
}

// Computes taylor expansion and substitute into the coeff
void taylor_expansion(uint32_t N) {
	int i;
	// Numerical value of epsilon
	double epsilon = 1.0e-10;
	// derivative computation
	derivative_calc(N);
	for (i = 0; i < N + 1; i++) {
		taylor_plus[i] = coefficients[i] + epsilon * derivative_coeffs[i];
		taylor_minus[i] = coefficients[i] - epsilon * derivative_coeffs[i];
	}
}

int singular_interpretation(uint32_t N) {
	// We get the calculum here
	int i, sp, sm, spm, zerocount;
	double sign, tested;
	printf(
			"\n ----- \nNumerical Taylor expansion proceeding...\n 0.01 values displayed below are numbers close to zero \n rounded for best print rendering purposes \n");
	taylor_expansion(N);
	routhtable plus = routh_filling(N, taylor_plus);
	routhtable minus = routh_filling(N, taylor_minus);
	plus = routh_calcus(plus);
	minus = routh_calcus(minus);
	printf("When epsilon is positive, we have: \n");
	print_rh(plus,0);
	printf("When epsilon is negative, we have : \n");
	print_rh(minus,0);

	zerocount = 0;
	// Sign detection
	if (plus.tab[0] > 0) {
		sign = 1.0;
	} else {
		sign = -1.0;
	};
	for (i = plus.n_row - 1; i >= 0; i--) {

		tested = sign * plus.tab[i][0];
		if (tested > 0) {
			sp = 1;
		} else {
			sp = -1;
		};
		tested = sign * minus.tab[i][0];
		if (tested > 0) {
			sm = 1;
		} else {
			sm = -1;
		};
		spm = sp + sm;
		if (spm == -2) {
			// Unconditionnal sign alternance = instability
			printf(
					"A sign change is persistant in the table for epsilon \n positive or negative. The system is unstable! \n");
			return 0;
		} else {
			if (spm == 0) {
				// Increments the number of epsilon-sign-conditionnal coefficient sign changes
				zerocount++;
			}
		}
	}
	printf(
			"The system is marginally stable, moreover there are %d pairs of poles on the imaginary axis. \n",
			zerocount);
	printf(
			"We know how many poles are on this axis thanks to the number of first column \n coefficient  sign changes between epsilon>0 and <0.\n");
	return 0;
}
